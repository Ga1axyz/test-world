new Vue({
    el: '#queryReport',
    data: {
        b1: 1,
        b2: 2,
        b3: 2,
        b4: 2,
        b5: 2,
        b6: 2,
        btn_able: 0,
        cancel: 0,
        acount: [
            {
                code: 1219395750,
                name: '阿波'
            },
            {
                code: 123456789,
                name: '志远'
            }
        ],
        resultshow: 1,
        examine: {
            columeName: ['序号',
                '代码',
                '项目名称',
                '结果',
                '单位',
                '参考值'],
            result: [{
                name: '谷丙转氨酶',
                code: 'GBT',
                result: 41,
                Company: 'U/L',
                consult: '0--31'
            },
            {
                name: '谷草转氨酶',
                code: 'GOT',
                result: 32,
                Company: 'U/L',
                consult: '0--31'
            },
            {
                name: '谷草/谷丙',
                code: 'GOT/GPT',
                result: 0.78,
                Company: ':l',
                consult: '1--2'
            },
            {
                name: 'r-谷氨酰转移酶',
                code: 'GGT',
                result: 12,
                Company: 'U/L',
                consult: '7--32'
            },
            {
                name: '碱性磷酸酶',
                code: 'ALP',
                result: 36,
                Company: 'U/L',
                consult: '34--114'
            },
            {
                name: '总蛋白',
                code: 'TP',
                result: 65,
                Company: 'g/L',
                consult: '60--87'
            },
            {
                name: '直接胆红素',
                code: 'DBIL',
                result: 4,
                Company: 'mol/L',
                consult: '0--6.84'
            }
            ]
        },
        inspection: {
            columeName: ['序号',
                '代码',
                '项目名称',
                '结果',
                '单位',
                '参考值'],
            result: [{
                name: '白细胞',
                code: 'WBC',
                result: 7.33,
                Company: '10^9/L',
                consult: '4-9'
            },
            {
                name: '红细胞',
                code: 'RBC',
                result: 4.76,
                Company: '10^12/L',
                consult: '43.5--5.5'
            },
            {
                name: '血红蛋白',
                code: 'HGB',
                result: 151,
                Company: 'g/L',
                consult: '110--160'
            },
            {
                name: '红细胞平均体积',
                code: 'MCT',
                result: 44.1,
                Company: '%',
                consult: '36--50'
            },
            {
                name: '平均血红蛋白量',
                code: 'MCH',
                result: 31.7,
                Company: 'pg',
                consult: '56--32'
            },
            {
                name: '平均血红蛋白浓度',
                code: 'MCH',
                result: 31.7,
                Company: 'pg',
                consult: '56--32'
            },
            {
                name: '血小板',
                code: 'MCH',
                result: 31.7,
                Company: 'pg',
                consult: '56--32'
            }
            ]
        },
        covid: {
            columeName: ['核酸检测结果:', '阳性'],
            result: []
        },
        showtable: {},
        name: null,
        code: null,
    },
    methods: {
        chosereport(i) {

            if (!this.btn_able) {
                alert('请输入就诊号和姓名')
            } else {
                this.resultshow = 0;
                if (i == 1) {
                    this.showtable = this.inspection;
                } else if (i == 2) {
                    this.showtable = this.examine
                } else if (i == 3) {
                    this.showtable = this.covid
                } else if (i == 4) {

                } else if (i == 5) {

                } else {

                }
            }
            console.log(this.showtable)
        },
        moveBack() {
            this.showtable = {}
            this.resultshow = 1;
        },
        search() {
            if (this.code == this.acount[0].code && this.name == this.acount[0].name) {
                this.b4 = 0;
                this.b5 = 0;
                this.b6 = 0;
                this.btn_able = 1
                this.cancel = 1;
            } else if (this.code == this.acount[1].code && this.name == this.acount[1].name) {
                this.b3 = 0;
                this.b4 = 0;
                this.b5 = 0;
                this.b6 = 0;
                this.btn_able = 1
                this.cancel = 1;

            } else {
                alert("请输入正确就诊号与姓名")
            }
        },
        cancelSearch() {
            this.name = null;
            this.code = null;
            this.b1 = 1,
                this.b2 = 2,
                this.b3 = 2,
                this.b4 = 2,
                this.b5 = 2,
                this.b6 = 2,
                this.cancel = 0
            this.btn_able = 0
        }
    }
})