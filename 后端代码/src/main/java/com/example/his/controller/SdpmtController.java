package com.example.his.controller;


import com.example.his.entity.Sdepartment;
import com.example.his.mapper.SdpmtMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
public class SdpmtController {
    @Autowired
    SdpmtMapper sdpmtMapper;

    //POST,创建一个父科室实体,ok,小写字段名
    @PostMapping("/api/Sdepartment")
    public ResponseEntity<Sdepartment> add_Sdpmt(@RequestBody Sdepartment sdepartment){
        //持久化参数对象
        //在save之前是没有id的
        sdpmtMapper.save(sdepartment);
        //save后生成了id
        return new ResponseEntity<Sdepartment>(
                sdepartment, HttpStatus.OK
        );
    }

    //DELETE,删除一个父科室实体,ok
    @DeleteMapping("/api/Sdepartment/{Sdid}")
    public ResponseEntity<String> delSdpmt(@PathVariable int Sdid){
        sdpmtMapper.remove(Sdid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //PUT,更新一个实体，按照Id更新,
    @PutMapping("/api/Sdepartment/{Sdid}")
    public ResponseEntity<Sdepartment> updateSdpmt(@PathVariable int Sdid,@RequestBody Sdepartment sdepartment){
        sdepartment.setSdid(Sdid);
        sdpmtMapper.update(sdepartment);
        return new ResponseEntity<Sdepartment>(sdepartment,HttpStatus.OK);
    }

    //GET,检索实体集合,ok
    @GetMapping("/api/Sdepartment")
    public ResponseEntity<List<Sdepartment>> findAllSdpmt(){
        List<Sdepartment> list= sdpmtMapper.findAll();
        return new ResponseEntity<List<Sdepartment>>(
                list, HttpStatus.OK);
    }
    //GET,根据id查找
    @GetMapping("/api/Sdepartment/{Sdid}")
    public ResponseEntity<Sdepartment> findSdpmtById(@PathVariable  int Sdid){
        Sdepartment sdepartment= sdpmtMapper.load(Sdid);
        return new ResponseEntity<Sdepartment>(
                sdepartment,HttpStatus.OK);
    }

    @GetMapping("/api/SdpmtByFid/{Fdid}")
    public ResponseEntity<List<Sdepartment>> findSdpmtByFId(@PathVariable  int Fdid){
        List<Sdepartment> list= sdpmtMapper.findFmatch(Fdid);
        return new ResponseEntity<List<Sdepartment>>(list,HttpStatus.OK);
    }
}
