package com.example.his.controller;

import com.example.his.entity.Doctor;
import com.example.his.mapper.DoctorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
public class DoctorController {
    @Autowired
    DoctorMapper doctorMapper;

    //POST,创建一个医生实体,ok,小写字段名
    @PostMapping("/api/Doctor")
    public ResponseEntity<Doctor> add_doctor(@RequestBody Doctor doctor){
        //持久化参数对象
        //在save之前是没有id的
        doctorMapper.save(doctor);
        //save后生成了id
        return new ResponseEntity<Doctor>(
                doctor, HttpStatus.OK
        );
    }

    //DELETE,删除一个父科室实体,ok
    @DeleteMapping("/api/Doctor/{Did}")
    public ResponseEntity<String> delDoctor(@PathVariable int Did){
        doctorMapper.remove(Did);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //PUT,更新一个实体，按照Id更新,ok
    @PutMapping("/api/Doctor/{Did}")
    public ResponseEntity<Doctor> updateDoctor(@PathVariable int Did,@RequestBody Doctor doctor){
        doctor.setDid(Did);
        doctorMapper.update(doctor);
        return new ResponseEntity<Doctor>(doctor,HttpStatus.OK);
    }

    //GET,检索实体集合,ok
    @GetMapping("/api/Doctor")
    public ResponseEntity<List<Doctor>> findAllDoctor(){
        List<Doctor> list= doctorMapper.findAll();
        return new ResponseEntity<List<Doctor>>(
                list, HttpStatus.OK);
    }
    //GET,根据id查找
    @GetMapping("/api/Doctor/{Did}")
    public ResponseEntity<Doctor> findDoctorById(@PathVariable  int Did){
        Doctor doctor= doctorMapper.load(Did);
        return new ResponseEntity<Doctor>(
                doctor,HttpStatus.OK);
    }



}
