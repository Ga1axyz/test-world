package com.example.his.controller;

import com.example.his.entity.Doctor;
import com.example.his.entity.Schedule;
import com.example.his.mapper.ScheduleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
public class ScheduleController {
    @Autowired
    ScheduleMapper scheduleMapper;

    //POST,创建一个实体,ok,小写字段名
    @PostMapping("/api/Schedule")
    public ResponseEntity<Schedule> add_Schedule(@RequestBody Schedule schedule){
        //持久化参数对象
        //在save之前是没有id的
        scheduleMapper.save(schedule);
        //save后生成了id
        return new ResponseEntity<Schedule>(
                schedule, HttpStatus.OK
        );
    }

    //DELETE,删除一个父科室实体,ok
    @DeleteMapping("/api/Schedule/{Sid}")
    public ResponseEntity<String> delSchedule(@PathVariable int Sid){
        scheduleMapper.remove(Sid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //PUT,更新一个实体，按照Id更新,ok
    @PutMapping("/api/Schedule/{Sid}")
    public ResponseEntity<Schedule> updateSchedule(@PathVariable int Sid,@RequestBody Schedule schedule){
        schedule.setSid(Sid);
        scheduleMapper.update(schedule);
        return new ResponseEntity<Schedule>(schedule,HttpStatus.OK);
    }

    //GET,检索实体集合,ok
    @GetMapping("/api/Schedule")
    public ResponseEntity<List<Schedule>> findAllSchedule(){
        List<Schedule> list= scheduleMapper.findAll();
        return new ResponseEntity<List<Schedule>>(
                list, HttpStatus.OK);
    }
    //GET,根据id查找
    @GetMapping("/api/Schedule/{Sid}")
    public ResponseEntity<Schedule> findScheduleById(@PathVariable  int Sid){
        Schedule schedule= scheduleMapper.load(Sid);
        return new ResponseEntity<Schedule>(
                schedule,HttpStatus.OK);
    }

}
