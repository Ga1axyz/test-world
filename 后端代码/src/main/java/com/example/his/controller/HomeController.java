package com.example.his.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

//由框架管理这个类,注解后扮演角色

/**
 * 控制器是Spring web MVC的核心：接收请求，完成响应
 *
 */
@CrossOrigin
//普通控制器,返回页面
@Controller
public class HomeController {
    @GetMapping("/")
    public String Home(){
        return "index.html";
    }
    @GetMapping("/patient")
    public String pat(){
        return "patient.html";
    }
    //@GetMapping("")

}
