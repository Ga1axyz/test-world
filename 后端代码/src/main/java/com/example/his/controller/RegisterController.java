package com.example.his.controller;

import com.example.his.entity.Doctor;
import com.example.his.entity.Register;
import com.example.his.mapper.RegisterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
public class RegisterController {
    @Autowired
    RegisterMapper registerMapper;

    //POST,创建一个医生实体,ok,小写字段名
    @PostMapping("/api/Register")
    public ResponseEntity<Register> add_Register(@RequestBody Register register){
        //持久化参数对象
        //在save之前是没有id的
        registerMapper.save(register);
        //save后生成了id
        return new ResponseEntity<Register>(
                register, HttpStatus.OK
        );
    }

    //DELETE,删除一个父科室实体,ok
    @DeleteMapping("/api/Register/{Rid}")
    public ResponseEntity<String> delRegister(@PathVariable int Rid){
        registerMapper.remove(Rid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //PUT,更新一个实体，按照Id更新,ok
    @PutMapping("/api/Register/{Rid}")
    public ResponseEntity<Register> updateRegister(@PathVariable int Rid,@RequestBody Register register){
        register.setRid(Rid);
        registerMapper.update(register);
        return new ResponseEntity<Register>(register,HttpStatus.OK);
    }

    //GET,检索实体集合,ok
    @GetMapping("/api/Register")
    public ResponseEntity<List<Register>> findAllRegister(){
        List<Register> list= registerMapper.findAll();
        return new ResponseEntity<List<Register>>(
                list, HttpStatus.OK);
    }
    //GET,根据id查找
    @GetMapping("/api/Register/{Rid}")
    public ResponseEntity<Register> findRegisterById(@PathVariable  int Rid){
        Register register= registerMapper.load(Rid);
        return new ResponseEntity<Register>(
                register,HttpStatus.OK);
    }
}
