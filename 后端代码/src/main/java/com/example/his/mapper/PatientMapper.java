package com.example.his.mapper;
import com.example.his.entity.Patient;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Mybatis是一个SQL映射框架，实现数据持久化
 * 调用方法时，更加方便
 */
@Mapper
public interface PatientMapper {
    //增，，，，创建一个实体
            //useGeneratedKeys =true这个表示插入数据之后返回一个自增的主键id给你对应实体类中的主键属性。
            //
    @Options(useGeneratedKeys = true,keyColumn = "id",keyProperty = "id")
    @Insert("insert into patient(name,nation,sex,cardType,idCard,birthday,phone) values(#{name},#{nation},#{sex},#{cardType},#{idCard},#{birthday},#{phone})")
    void save(Patient patient);

    //查，，，，根据id查找一个就诊人实体
    @Select("select * from patient where id = #{id}")
//    //名字不完全匹配的做映射,把数据库里面的id_card映射成idCard
//    @Results(
//            id="patientResult",//为该映射取名
//            value={
//             @Result(column = "id_card",property = "idCard")
//    })
    Patient load(int id);
    //查找多个,返回集合
    @Select("select * from patient")
    //使用映射名把数据库里面的id_card映射成idCard
    //@ResultMap("patientResult")
    List<Patient> findAll();


    //改，，，，更新实体属性
    @Update("update patient set nation=#{nation},sex=#{sex},cardType=#{cardType},idCard=#{idCard}" +
            ",birthday=#{birthday},phone=#{phone} where id=#{id}")
    void update(Patient patient);

     //删，，，，按指定id删除实体
     @Delete("delete from patient where id=#{id}")
     void remove(int id);
}
