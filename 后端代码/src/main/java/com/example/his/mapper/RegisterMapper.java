package com.example.his.mapper;


import com.example.his.entity.Register;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface RegisterMapper {
    //增，，，，创建一个实体
    //useGeneratedKeys =true这个表示插入数据之后返回一个自增的主键id给你对应实体类中的主键属性。
    @Options(useGeneratedKeys = true,keyColumn = "Rid",keyProperty = "Rid")
    @Insert("insert into register(patientid,doctorid,Rdate,apm) values(#{patientid}" +
            ",#{doctorid},#{Rdate},#{apm})")
    void save(Register register);

    //删，，，，按指定id删除实体
    @Delete("delete from register where Rid=#{Rid}")
    void remove(int Rid);

    //改，，，，更新实体属性
    @Update("update register set patientid=#{patientid},doctorid=#{doctorid}" +
            ",Rdate=#{Rdate},apm=#{apm} where Rid=#{Rid}")
    void update(Register register);

    //查，，，，根据id查找一个实体
    @Select("select * from register where Rid = #{Rid}")
    Register load(int Rid);
    //查找多个,返回集合
    @Select("select * from register")
    List<Register> findAll();
}
