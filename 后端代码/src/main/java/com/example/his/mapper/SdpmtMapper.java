package com.example.his.mapper;

import com.example.his.entity.Sdepartment;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface SdpmtMapper {
    //增，，，，创建一个实体
    //useGeneratedKeys =true这个表示插入数据之后返回一个自增的主键id给你对应实体类中的主键属性。
    @Options(useGeneratedKeys = true,keyColumn = "Sdid",keyProperty = "Sdid")
    @Insert("insert into sdepartment(Sdname,Fdid,Sdescription) values(#{Sdname},#{Fdid},#{Sdescription})")
    void save(Sdepartment sdepartment);

    //删，，，，按指定id删除实体
    @Delete("delete from sdepartment where Sdid=#{Sdid}")
    void remove(int Sdid);

    //改，，，，更新实体属性
    @Update("update sdepartment set Sdname=#{Sdname},Fdid=#{Fdid},Sdescription=#{Sdescription} where Sdid=#{Sdid}")
    void update(Sdepartment sdepartment);

    //查，，，，根据id查找一个就诊人实体
    @Select("select * from sdepartment where Sdid = #{Sdid}")
    Sdepartment load(int Sdid);
    //查找多个,返回集合
    @Select("select * from sdepartment")
    List<Sdepartment> findAll();

    //根据父id 查找匹配项
    @Select("select * from sdepartment where Fdid = #{Fdid}")
    List<Sdepartment> findFmatch(int Fdid);

}
