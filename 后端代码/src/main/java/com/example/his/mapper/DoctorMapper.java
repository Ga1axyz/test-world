package com.example.his.mapper;

import com.example.his.entity.Doctor;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DoctorMapper {
    //增，，，，创建一个实体
    //useGeneratedKeys =true这个表示插入数据之后返回一个自增的主键id给你对应实体类中的主键属性。
    @Options(useGeneratedKeys = true,keyColumn = "Did",keyProperty = "Did")
    @Insert("insert into doctor(Dname,Dpmtid,Dphone,sDescription,bDescription) values(#{Dname}" +
            ",#{Dpmtid},#{Dphone},#{sDescription},#{bDescription})")
    void save(Doctor Doctor);

    //删，，，，按指定id删除实体
    @Delete("delete from doctor where Did=#{Did}")
    void remove(int Did);

    //改，，，，更新实体属性
    @Update("update doctor set Dpmtid=#{Dpmtid},Dphone=#{Dphone}" +
            ",sDescription=#{sDescription},bDescription=#{bDescription} where Did=#{Did}")
    void update(Doctor doctor);

    //查，，，，根据id查找一个就诊人实体
    @Select("select * from doctor where Did = #{Did}")
    Doctor load(int Did);
    //查找多个,返回集合
    @Select("select * from doctor")
    List<Doctor> findAll();
}
