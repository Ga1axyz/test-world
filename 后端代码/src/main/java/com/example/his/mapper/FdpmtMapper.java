package com.example.his.mapper;

import com.example.his.entity.Fdepartment;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface FdpmtMapper {
    //增，，，，创建一个实体
    //useGeneratedKeys =true这个表示插入数据之后返回一个自增的主键id给你对应实体类中的主键属性。
    @Options(useGeneratedKeys = true,keyColumn = "Fdid",keyProperty = "Fdid")
    @Insert("insert into fdepartment(Fdname,Fdescription) values(#{Fdname},#{Fdescription})")
    void save(Fdepartment fdepartment);


    //删，，，，按指定id删除实体
    @Delete("delete from fdepartment where Fdid=#{Fdid}")
    void remove(int Fdid);


    //改，，，，更新实体属性
    @Update("update fdepartment set Fdname=#{Fdname},Fdescription=#{Fdescription} where Fdid=#{Fdid}")
    void update(Fdepartment Fdepartment);


    //查，，，，根据id查找一个就诊人实体
    @Select("select * from fdepartment where Fdid = #{Fdid}")
    Fdepartment load(int Fdid);
    //查找多个,返回集合
    @Select("select * from fdepartment")
    List<Fdepartment> findAll();



}
