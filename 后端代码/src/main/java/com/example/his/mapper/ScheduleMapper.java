package com.example.his.mapper;

import com.example.his.entity.Schedule;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ScheduleMapper {
    //增，，，，创建一个实体
    //useGeneratedKeys =true这个表示插入数据之后返回一个自增的主键id给你对应实体类中的主键属性。
    @Options(useGeneratedKeys = true,keyColumn = "Sid",keyProperty = "Sid")
    @Insert("insert into schedule(Doctorid,Sdate,CurrentNum,MaxNum,apm) values(#{Doctorid}" +
            ",#{Sdate},#{CurrentNum},#{MaxNum},#{apm})")
    void save(Schedule schedule);

    //删，，，，按指定id删除实体
    @Delete("delete from schedule where Sid=#{Sid}")
    void remove(int Sid);

    //改，，，，更新实体属性
    @Update("update schedule set Doctorid=#{Doctorid},Sdate=#{Sdate}" +
            ",CurrentNum=#{CurrentNum},MaxNum=#{MaxNum},apm=#{apm} where Sid=#{Sid}")
    void update(Schedule schedule);

    //查，，，，根据id查找一个就诊人实体
    @Select("select * from schedule where Sid = #{Sid}")
    Schedule load(int Sid);
    //查找多个,返回集合
    @Select("select * from schedule")
    List<Schedule> findAll();




}
