package com.example.his.entity;

public class Fdepartment {
//+--------------+------------------+------+-----+---------+----------------+
//            | Field        | Type             | Null | Key | Default | Extra          |
//            +--------------+------------------+------+-----+---------+----------------+
//            | Fdid         | int(20) unsigned | NO   | PRI | NULL    | auto_increment |
//            | Fdname       | varchar(20)      | NO   |     | NULL    |                |
//            | Fdescription | varchar(255)     | YES  |     | NULL    |                |
//            +--------------+------------------+------+-----+---------+----------------+

    int Fdid;
    String Fdname;
    String Fdescription;

    public int getFdid() {
        return Fdid;
    }

    public void setFdid(int fdid) {
        Fdid = fdid;
    }

    public String getFdname() {
        return Fdname;
    }

    public void setFdname(String fdname) {
        Fdname = fdname;
    }

    public String getFdescription() {
        return Fdescription;
    }

    public void setFdescription(String fdescription) {
        Fdescription = fdescription;
    }
}
