package com.example.his.entity;

import java.util.List;

public class ShowDpmt {
    int  id;
    String name;
    List<Sdepartment> detail;
    int subAmount;

    public List<Sdepartment> getDetail() {
        return detail;
    }

    public void setDetail(List<Sdepartment> detail) {
        this.detail = detail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getSubAmount() {
        return subAmount;
    }

    public void setSubAmount(int subAmount) {
        this.subAmount = subAmount;
    }
}
