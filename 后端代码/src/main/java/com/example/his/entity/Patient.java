package com.example.his.entity;

/**
 * 就诊人表结构
 * | Field    | Type             | Null | Key | Default                  | Extra          |
 * +----------+------------------+------+-----+--------------------------+----------------+
 * | id       | int(20) unsigned | NO   | PRI | NULL                     | auto_increment |
 * | name     | varchar(20)      | NO   |     | NULL                     |                |
 * | nation   | varchar(10)      | NO   |     | 汉族                     |                |
 * | sex      | varchar(5)       | NO   |     | NULL                     |                |
 * | cardType | varchar(25)      | NO   |     | 中华人民共和国居民身份证 |                |
 * | idCard   | varchar(20)      | NO   |     | NULL                     |                |
 * | birthday | date             | YES  |     | NULL                     |                |
 * | phone    | varchar(11)      | NO   |     | NULL                     |
 */

public class Patient {
    //就诊人id
    int id;
    //就诊人姓名
    String name;
    //民族
    String nation;
    //性别
    String sex;
    //身份证类型
    String cardType;
    //就诊人身份证号码
    String idCard;
    //出生日期
    String birthday;
    //就诊人电话号码
    String phone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
