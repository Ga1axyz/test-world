package com.example.his.entity;

public class Register {
//+-----------+--------------+------+-----+---------+----------------+
//| Field     | Type         | Null | Key | Default | Extra          |
//+-----------+--------------+------+-----+---------+----------------+
//| Rid       | int unsigned | NO   | PRI | NULL    | auto_increment |
//| patientid | int unsigned | YES  | MUL | NULL    |                |
//| doctorid  | int unsigned | YES  | MUL | NULL    |                |
//| Rdate     | date         | NO   |     | NULL    |                |
//| apm       |enum('pm','am')| NO   |     | NULL    |                |
//+-----------+--------------+------+-----+---------+----------------+

    int Rid;
    int patientid;
    int doctorid;
    String Rdate;
    String apm;

    public int getRid() {
        return Rid;
    }

    public void setRid(int rid) {
        Rid = rid;
    }

    public int getPatientid() {
        return patientid;
    }

    public void setPatientid(int patientid) {
        this.patientid = patientid;
    }

    public int getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(int doctorid) {
        this.doctorid = doctorid;
    }

    public String getRdate() {
        return Rdate;
    }

    public void setRdate(String rdate) {
        Rdate = rdate;
    }

    public String getApm() {
        return apm;
    }

    public void setApm(String apm) {
        this.apm = apm;
    }
}
