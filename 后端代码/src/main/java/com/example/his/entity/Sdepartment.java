package com.example.his.entity;

public class Sdepartment {
//    +--------------+------------------+------+-----+---------+----------------+
//            | Field        | Type             | Null | Key | Default | Extra          |
//            +--------------+------------------+------+-----+---------+----------------+
//            | Sdid         | int(20) unsigned | NO   | PRI | NULL    | auto_increment |
//            | Sdname       | varchar(25)      | NO   |     | NULL    |                |
//            | Fdid         | int(20) unsigned | NO   | MUL | NULL    |                |
//            | Sdescription | varchar(255)     | YES  |     | NULL    |                |
//            +--------------+------------------+------+-----+---------+----------------+


    int Sdid;
    String Sdname;
    int Fdid;
    String Sdescription;

    public int getSdid() {
        return Sdid;
    }

    public void setSdid(int sdid) {
        Sdid = sdid;
    }

    public String getSdname() {
        return Sdname;
    }

    public void setSdname(String sdname) {
        Sdname = sdname;
    }

    public int getFdid() {
        return Fdid;
    }

    public void setFdid(int fdid) {
        Fdid = fdid;
    }

    public String getSdescription() {
        return Sdescription;
    }

    public void setSdescription(String sdescription) {
        Sdescription = sdescription;
    }
}
