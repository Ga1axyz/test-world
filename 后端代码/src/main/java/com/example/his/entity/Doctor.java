package com.example.his.entity;

public class Doctor {
//+--------------+------------------+------+-----+---------+----------------+
//            | Field        | Type             | Null | Key | Default | Extra          |
//            +--------------+------------------+------+-----+---------+----------------+
//            | Did          | int(20) unsigned | NO   | PRI | NULL    | auto_increment |
//            | Dname        | varchar(20)      | NO   |     | NULL    |                |
//            | Dpmtid       | int(20) unsigned | NO   | MUL | NULL    |                |
//            | Dphone       | varchar(11)      | NO   |     | NULL    |                |
//            | sDescription | varchar(80)      | YES  |     | NULL    |                |
//            | bDescription | varchar(255)     | YES  |     | NULL    |                |
//            +--------------+------------------+------+-----+---------+----------------+
    int Did;
    String Dname;
    int Dpmtid ;
    String Dphone;
    String sDescription;
    String bDescription;

    public int getDid() {
        return Did;
    }

    public void setDid(int did) {
        Did = did;
    }

    public String getDname() {
        return Dname;
    }

    public void setDname(String dname) {
        Dname = dname;
    }

    public int getDpmtid() {
        return Dpmtid;
    }

    public void setDpmtid(int dpmtid) {
        Dpmtid = dpmtid;
    }

    public String getDphone() {
        return Dphone;
    }

    public void setDphone(String dphone) {
        Dphone = dphone;
    }

    public String getsDescription() {
        return sDescription;
    }

    public void setsDescription(String sDescription) {
        this.sDescription = sDescription;
    }

    public String getbDescription() {
        return bDescription;
    }

    public void setbDescription(String bDescription) {
        this.bDescription = bDescription;
    }
}
