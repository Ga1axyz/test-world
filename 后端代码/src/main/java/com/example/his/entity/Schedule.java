package com.example.his.entity;

import java.util.Date;

public class Schedule {
//    +------------+-----------------+------+-----+---------+----------------+
//            | Field      | Type            | Null | Key | Default | Extra          |
//            +------------+-----------------+------+-----+---------+----------------+
//            | Sid        | int unsigned    | NO   | PRI | NULL    | auto_increment |
//            | Doctorid   | int unsigned    | YES  | MUL | NULL    |                |
//            | Sdate      | date            | YES  |     | NULL    |                |
//            | CurrentNum | int unsigned    | YES  |     | NULL    |                |
//            | MaxNum     | int unsigned    | YES  |     | NULL    |                |
//            | apm        | enum('pm','am') | YES  |     | NULL    |                |
//            +------------+-----------------+------+-----+---------+----------------+

    int Sid;
    int Doctorid;
    String Sdate;
    int CurrentNum;
    int MaxNum;
    String apm;

    public int getSid() {
        return Sid;
    }

    public void setSid(int sid) {
        Sid = sid;
    }

    public int getDoctorid() {
        return Doctorid;
    }

    public void setDoctorid(int doctorid) {
        Doctorid = doctorid;
    }

    public String getSdate() {
        return Sdate;
    }

    public void setSdate(String sdate) {
        Sdate = sdate;
    }

    public int getCurrentNum() {
        return CurrentNum;
    }

    public void setCurrentNum(int currentNum) {
        CurrentNum = currentNum;
    }

    public int getMaxNum() {
        return MaxNum;
    }

    public void setMaxNum(int maxNum) {
        MaxNum = maxNum;
    }

    public String getApm() {
        return apm;
    }

    public void setApm(String apm) {
        this.apm = apm;
    }
}
